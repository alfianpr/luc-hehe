SELECT 
    name,
    link,
    plu,
    CAST(REPLACE(discount, '%', '') AS REAL) AS discount,
    CAST(REPLACE(REPLACE(original_price, 'Rp ', ''), '.', '') AS REAL) AS original_price,
    CAST(REPLACE(REPLACE(discounted_price, 'Rp ', ''), '.', '') AS REAL) AS discounted_price,
    description,
    store_info,
    category
FROM 
    {table_name}
WHERE 
    CAST(REPLACE(REPLACE(discounted_price, 'Rp ', ''), '.', '') AS REAL) > 0 AND
    CAST(REPLACE(REPLACE(original_price, 'Rp ', ''), '.', '') AS REAL) > 0;
